# Gamification Website

### Rust Logs

```zsh
RUST_LOG=debug cargo run
```

### Diesel Migrations

Should be ran on a fresh db of when migration is added

```zsh
diesel migration run
```

### Update
Aware of the password in the docker-compose file. This was a test docker image and was not used when the project was
deployed. The pass was generated and has no significant meaning nor will ever be reused.
