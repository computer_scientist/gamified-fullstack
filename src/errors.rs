use std::fmt;

use actix_web::{
    error,
    http::{header::ContentType, StatusCode},
    HttpResponse,
};
use derive_more::{Display, Error};

// Registration Error Enum
#[derive(Debug)]
pub enum RegistrationError {
    EmailMismatch,
    EmailExists,
    PasswordMismatch,
    WeakPassword,
    UserExists,
    PasswordHashingError,
    DatabaseError,
}

impl fmt::Display for RegistrationError {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        use RegistrationError::*;

        let message = match self {
            EmailMismatch => "Emails do not match.",
            EmailExists => "Email already exists",
            PasswordMismatch => "Passwords do not match.",
            WeakPassword => "Password is too weak.",
            UserExists => "User already exists.",
            PasswordHashingError => "There was an error hashing password.",
            DatabaseError => "An internal error occurred. Please try again later.",
        };

        f.write_str(message)
    }
}

impl std::error::Error for RegistrationError {}

// Argon2Error Struct
pub struct Argon2Error(argon2::password_hash::Error);

#[allow(dead_code)]
impl Argon2Error {
    pub fn new(err: argon2::password_hash::Error) -> Self {
        Argon2Error(err)
    }
}

impl fmt::Debug for Argon2Error {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "Argon2Error")
    }
}

impl fmt::Display for Argon2Error {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(
            f,
            "An error occurred with password processing. Please try again later."
        )
    }
}

impl std::error::Error for Argon2Error {}

// Authentication
#[derive(Debug)]
pub enum LoginError {
    InvalidCredentials,
    TooManyFailedAttempts,
    UserNotFound,
    DatabaseError,
}

// User Response Errors
#[derive(Debug, Display, Error)]
pub enum UserFacingError {
    #[display(fmt = "An internal error occurred. Please try again later.")]
    InternalError,
}

impl error::ResponseError for UserFacingError {
    fn status_code(&self) -> StatusCode {
        match *self {
            UserFacingError::InternalError => StatusCode::INTERNAL_SERVER_ERROR,
        }
    }

    fn error_response(&self) -> HttpResponse {
        HttpResponse::build(self.status_code())
            .insert_header(ContentType::html())
            .body(self.to_string())
    }
}
