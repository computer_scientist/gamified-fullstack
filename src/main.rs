mod authenticate;
mod database;
mod errors;
mod handlers;
mod leaderboard;
mod registration;
mod routes;

use crate::database::db;
use actix_service::Service;
use actix_session::{storage::RedisActorSessionStore, SessionMiddleware};
use actix_web::{cookie::Key, middleware::Logger, web, App, HttpServer};
use openssl::ssl::{SslAcceptor, SslFiletype, SslMethod}; // For Non-Reverse proxy

/*
   For non-reverse proxy setup
*/
#[actix_rt::main]
async fn main() -> std::io::Result<()> {
    env_logger::init();

    // Build ssl
    let mut builder = SslAcceptor::mozilla_intermediate(SslMethod::tls()).unwrap();
    builder
        .set_private_key_file("key.pem", SslFiletype::PEM)
        .unwrap();
    builder.set_certificate_chain_file("cert.pem").unwrap();

    // Session Management
    let secret_key = Key::generate();
    let redis_connection_string = "127.0.0.1:6379";

    // Create Database pool
    let pool = db::create_pool();

    HttpServer::new(move || {
        App::new()
            .wrap(SessionMiddleware::new(
                RedisActorSessionStore::new(redis_connection_string),
                secret_key.clone(),
            ))
            .wrap(Logger::default())
            .wrap_fn(|req, srv| {
                let fut = srv.call(req);
                async {
                    let res = fut.await;
                    if let Err(e) = &res {
                        log::error!("Server error: {:?}", e);
                    }
                    res
                }
            })
            .app_data(web::Data::new(pool.clone()))
            .configure(routes::app_config)
            .default_service(web::get().to(handlers::handler_not_found))
    })
    .bind_openssl("0.0.0.0:8080", builder)?
    .run()
    .await
}

/*
   For Reverse Proxy Setup
*/
// #[actix_rt::main]
// async fn main() -> std::io::Result<()> {
//     env_logger::init();
//
//     // Session Management
//     let secret_key = Key::generate();
//     let redis_connection_string = "127.0.0.1:6379";
//
//     // Create Database pool
//     let pool = db::create_pool();
//
//     HttpServer::new(move || {
//         App::new()
//             .wrap(SessionMiddleware::new(
//                 RedisActorSessionStore::new(redis_connection_string),
//                 secret_key.clone(),
//             ))
//             .wrap(Logger::default())
//             .wrap_fn(|req, srv| {
//                 let fut = srv.call(req);
//                 async {
//                     let res = fut.await;
//                     if let Err(e) = &res {
//                         log::error!("Server error: {:?}", e);
//                     }
//                     res
//                 }
//             })
//             .app_data(web::Data::new(pool.clone()))
//             .configure(routes::app_config)
//             .default_service(web::get().to(handlers::handler_not_found))
//     })
//     .bind("127.0.0.1:8080")?
//     .run()
//     .await
// }
