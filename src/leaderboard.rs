use crate::database::db::DbPool;
use crate::database::forms::PointsFormData;
use crate::database::models::LeaderboardPoints;
use crate::database::schema::leaderboard_points;
use crate::database::schema::leaderboard_points::dsl::*;
// use crate::database::schema::points::user_id;
use crate::database::schema::users;
// use crate::database::schema::users::dsl::*;
use crate::errors::UserFacingError;
use actix_session::Session;
use actix_web::HttpResponse;
// use diesel::dsl::now;
use diesel::prelude::*;
use diesel::r2d2::{ConnectionManager, PooledConnection};
use diesel::{PgConnection, QueryDsl, RunQueryDsl};
// use diesel::QueryResult;
use crate::database::forms::PlayerPoints;

pub async fn leaderboard_entry(
    form: PointsFormData,
    pool: &DbPool,
    session: &Session,
) -> Result<HttpResponse, UserFacingError> {
    // Set variables from form data
    let points = form.points;
    let activity_name = form.game_name;

    // Set variables from session and handle session lookup gracefully
    let other_user_id = if let Ok(Some(other_user_id)) = session.get::<i32>("user_id") {
        other_user_id
    } else {
        log::error!("User ID not found in session when posting to leaderboard");
        return Ok(HttpResponse::BadRequest().body("Unable to post points"));
    };

    let mut conn = pool.get().map_err(|_| {
        log::error!("Failed to get a database pool connection from the leaderboard_entry");
        UserFacingError::InternalError
    })?;

    let mut no_need_to_update_db: bool = false;

    match get_leaderboard_entry(other_user_id, &mut conn) {
        Ok(entry) => {
            // If an entry exists, update it
            let new_points: i64 = match activity_name.as_str() {
                "game1" => {
                    if points > entry.game1 {
                        points
                    } else {
                        no_need_to_update_db = true;
                        entry.game1
                    }
                }
                "game2" => {
                    if points > entry.game2 {
                        points
                    } else {
                        no_need_to_update_db = true;
                        entry.game2
                    }
                }
                "science" => points + entry.science,
                _ => return Err(UserFacingError::InternalError),
            };
            if no_need_to_update_db {
                log::error!("User non high score");
            } else {
                update_points(other_user_id, &activity_name, new_points, &mut conn)?;
            }
        }
        Err(_) => {
            log::error!("Leaderboard entry failed");
        }
    }
    Ok(HttpResponse::Ok().finish())
}

fn get_leaderboard_entry(
    other_user_id: i32,
    conn: &mut PooledConnection<ConnectionManager<PgConnection>>,
) -> Result<LeaderboardPoints, UserFacingError> {
    let user_leaderboard_entries: LeaderboardPoints = leaderboard_points
        .filter(user_id.eq(other_user_id))
        .first(conn)
        .map_err(|_| {
            log::error!("Failed to get user id from the leaderboard_points table.");
            UserFacingError::InternalError
        })?;

    Ok(user_leaderboard_entries)
}

fn update_points(
    other_user_id: i32,
    activity_name: &str,
    new_points: i64,
    conn: &mut PooledConnection<ConnectionManager<PgConnection>>,
) -> Result<usize, UserFacingError> {
    let update_result = match activity_name {
        "game1" => diesel::update(leaderboard_points)
            .filter(user_id.eq(other_user_id))
            .set(game1.eq(new_points))
            .execute(conn),
        "game2" => diesel::update(leaderboard_points)
            .filter(user_id.eq(other_user_id))
            .set(game2.eq(new_points))
            .execute(conn),
        "science" => diesel::update(leaderboard_points)
            .filter(user_id.eq(other_user_id))
            .set(science.eq(new_points))
            .execute(conn),
        _ => return Err(UserFacingError::InternalError),
    };

    update_result.map_err(|_| {
        log::error!("Failed to update points in the leaderboard_points table");
        UserFacingError::InternalError
    })
}

pub async fn retrieve_top_players(
    subject: &str,
    pool: &DbPool,
) -> Result<Vec<PlayerPoints>, UserFacingError> {
    let mut conn = pool.get().map_err(|_| {
        log::error!("Failed to get a database pool when retrieving top players;");
        UserFacingError::InternalError
    })?;

    let update_result = match subject {
        "game1" => users::table
            .inner_join(leaderboard_points::table.on(users::id.eq(user_id)))
            .select((users::username, game1))
            .order_by(game1.desc())
            .limit(20)
            .load::<PlayerPoints>(&mut conn),
        "game2" => users::table
            .inner_join(leaderboard_points::table.on(users::id.eq(user_id)))
            .select((users::username, game2))
            .order_by(game2.desc())
            .limit(20)
            .load::<PlayerPoints>(&mut conn),
        "science" => users::table
            .inner_join(leaderboard_points::table.on(users::id.eq(user_id)))
            .select((users::username, science))
            .order_by(science.desc())
            .limit(20)
            .load::<PlayerPoints>(&mut conn),
        _ => return Err(UserFacingError::InternalError),
    };

    update_result.map_err(|e| {
        log::error!(
            "Failed to update points in the leaderboard_points table {:?}",
            e
        );
        UserFacingError::InternalError
    })
}
