use crate::database::models::User;
use crate::database::models::{InsertLeaderboardPoints, NewUser};
use crate::db::DbPool;
use crate::errors::RegistrationError;
use diesel::prelude::*;
use diesel::{PgConnection, RunQueryDsl};
use regex::Regex;

use crate::database::forms::RegisterFormData;
// use crate::database::schema::leaderboard_points;
use crate::database::schema::leaderboard_points::dsl::*;
use crate::database::schema::users::dsl::*;
use crate::database::schema::users::email;
use argon2::password_hash::rand_core;
use argon2::{
    password_hash::{PasswordHasher, SaltString},
    Argon2,
};
use diesel::r2d2::{ConnectionManager, PooledConnection};
use rand_core::OsRng;

pub async fn register_user(
    user_data: RegisterFormData,
    pool: &DbPool,
) -> Result<User, RegistrationError> {
    if user_data.email != user_data.confirm_email {
        return Err(RegistrationError::EmailMismatch);
    }
    if user_data.password != user_data.confirm_password {
        return Err(RegistrationError::PasswordMismatch);
    }
    if !is_password_strong(&user_data.password) {
        return Err(RegistrationError::WeakPassword);
    }
    let mut connection = pool.get().map_err(|e| {
        log::error!("Failed to get a connection from the pool: {:?}", e);
        RegistrationError::DatabaseError
    })?;
    if check_user_exists(&mut connection, &user_data.username)? {
        return Err(RegistrationError::UserExists);
    }
    if check_email_exists(&mut connection, &user_data.email)? {
        return Err(RegistrationError::EmailExists);
    }
    create_user(&mut connection, &user_data)
}

fn is_password_strong(other_password: &str) -> bool {
    let length = other_password.chars().count() >= 8;
    let digits = other_password.chars().any(|c| c.is_digit(10));
    let uppercase = other_password.chars().any(|c| c.is_uppercase());
    let lowercase = other_password.chars().any(|c| c.is_lowercase());

    let special_chars = Regex::new(r"[!@#^(){};/$%&*?:|<>]").unwrap();
    let special = special_chars.is_match(other_password);

    length && digits && uppercase && lowercase && special
}

fn check_user_exists(
    connection: &mut PooledConnection<ConnectionManager<PgConnection>>,
    user_name: &str,
) -> Result<bool, RegistrationError> {
    let result = users
        .filter(username.eq(user_name))
        .load::<User>(connection)
        .map_err(|_| {
            log::error!("Unable to check if user exists.");
            diesel::NotFound
        });
    match result {
        Ok(user) => {
            if user.is_empty() {
                Ok(false)
            } else {
                Ok(true)
            }
        }
        Err(diesel::NotFound) => Ok(false),

        Err(_) => Err(RegistrationError::DatabaseError),
    }
}

fn check_email_exists(
    connection: &mut PooledConnection<ConnectionManager<PgConnection>>,
    e_mail: &str,
) -> Result<bool, RegistrationError> {
    let result = users.filter(email.eq(e_mail)).first::<User>(connection);

    match result {
        Ok(_) => Ok(true),
        Err(diesel::NotFound) => Ok(false),
        Err(_) => Err(RegistrationError::DatabaseError),
    }
}

fn create_user(
    connection: &mut PooledConnection<ConnectionManager<PgConnection>>,
    user_data: &RegisterFormData,
) -> Result<User, RegistrationError> {
    let password_hash = hash_password(&user_data.password)?;

    let new_user = NewUser {
        username: user_data.username.clone(),
        email: user_data.email.clone(),
        password: password_hash,
        failed_login_attempts: 0,
    };

    let inserted_user: User = diesel::insert_into(users)
        .values(&new_user)
        .get_result(connection)
        .map_err(|_e| {
            log::error!("Unable to create user when inserting into users table");
            RegistrationError::DatabaseError
        })?;

    let initial_leaderboard_points = InsertLeaderboardPoints {
        game1: 0,
        game2: 0,
        science: 0,
    };

    diesel::insert_into(leaderboard_points)
        .values((
            user_id.eq(inserted_user.id),
            game1.eq(initial_leaderboard_points.game1),
            game2.eq(initial_leaderboard_points.game2),
            science.eq(initial_leaderboard_points.science),
        ))
        .execute(connection)
        .map_err(|_e| {
            log::error!("Unable to insert initial leaderboard data upon user creation");
            RegistrationError::DatabaseError
        })?;
    Ok(inserted_user)
}

pub fn hash_password(other_password: &str) -> Result<String, RegistrationError> {
    let salt = SaltString::generate(&mut OsRng);

    let argon2 = Argon2::default();

    let password_hash = argon2
        .hash_password(other_password.as_bytes(), &salt)
        .map_err(|_| RegistrationError::PasswordHashingError)?;

    Ok(password_hash.to_string())
}
