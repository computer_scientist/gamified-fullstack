use crate::database::forms::{
    LoginFormData, MathFormData, PointsFormData, RegisterFormData, UsernameResponse,
};

use crate::db::DbPool;
use crate::errors::UserFacingError;
use crate::leaderboard::leaderboard_entry;
use crate::leaderboard::retrieve_top_players;
use crate::{authenticate, registration};
use actix_files;
use actix_session::{Session, SessionExt};
use actix_web::http::header;
use actix_web::http::header::LOCATION;
use actix_web::{web, Error, HttpRequest, HttpResponse, Responder, Result};

#[allow(dead_code)]
pub async fn handler_index(
    req: HttpRequest,
    session: Session,
) -> Result<HttpResponse, UserFacingError> {
    if let Some(_user_id) = req.get_session().get::<usize>("user_id").unwrap_or(None) {
        let activity_name = session.get::<String>("activity_name").unwrap_or(None);

        match activity_name {
            Some(ref name) if name == "elements_lesson" => {
                let file =
                    actix_files::NamedFile::open("static/Elements_Lesson.html").map_err(|_| {
                        log::error!("Failed to open elements_lesson file");
                        UserFacingError::InternalError
                    })?;
                Ok(file.into_response(&req))
            }
            _ => {
                let file =
                    actix_files::NamedFile::open("static/html/index.html").map_err(|_| {
                        log::error!("Failed to open landing index file");
                        UserFacingError::InternalError
                    })?;
                Ok(file.into_response(&req))
            }
        }
    } else {
        let file = actix_files::NamedFile::open("static/html/index.html").map_err(|_| {
            log::error!("Failed to open landing index file");
            UserFacingError::InternalError
        })?;
        Ok(file.into_response(&req))
    }
}

#[allow(dead_code)]
pub async fn handler_about_us() -> Result<actix_files::NamedFile, UserFacingError> {
    actix_files::NamedFile::open("static/html/about-us.html").map_err(|_| {
        log::error!("Failed to open about-us.html file");
        UserFacingError::InternalError
    })
}

#[allow(dead_code)]
pub async fn handler_account(req: HttpRequest) -> Result<HttpResponse, UserFacingError> {
    if let Some(_) = req.get_session().get::<usize>("user_id").unwrap_or(None) {
        let file = actix_files::NamedFile::open("static/html/account.html").map_err(|_| {
            log::error!("Failed to open account.html file");
            UserFacingError::InternalError
        })?;
        Ok(file.into_response(&req))
    } else {
        Ok(HttpResponse::Found()
            .append_header((LOCATION, "/login"))
            .finish())
    }
}

#[allow(dead_code)]
pub async fn handler_activities(
    req: HttpRequest,
    session: Session,
) -> Result<HttpResponse, UserFacingError> {
    session.remove("activity_name");
    if let Some(_) = req.get_session().get::<usize>("user_id").unwrap_or(None) {
        let file = actix_files::NamedFile::open("static/html/activities.html").map_err(|_| {
            log::error!("Failed to open activities.html file");
            UserFacingError::InternalError
        })?;
        Ok(file.into_response(&req))
    } else {
        Ok(HttpResponse::Found()
            .append_header((LOCATION, "/login"))
            .finish())
    }
}

#[allow(dead_code)]
pub async fn handler_activity(req: HttpRequest) -> Result<HttpResponse, UserFacingError> {
    if let Some(_) = req.get_session().get::<usize>("user_id").unwrap_or(None) {
        let file = actix_files::NamedFile::open("static/html/activities.html").map_err(|_| {
            log::error!("Failed to open activity.html file");
            UserFacingError::InternalError
        })?;
        Ok(file.into_response(&req))
    } else {
        Ok(HttpResponse::Found()
            .append_header((LOCATION, "/activity"))
            .finish())
    }
}

#[allow(dead_code)]
pub async fn handler_biology_activities(req: HttpRequest) -> Result<HttpResponse, UserFacingError> {
    if let Some(_) = req.get_session().get::<usize>("user_id").unwrap_or(None) {
        let file =
            actix_files::NamedFile::open("static/html/biology-activities.html").map_err(|_| {
                log::error!("Failed to open biological-activities.html file");
                UserFacingError::InternalError
            })?;
        Ok(file.into_response(&req))
    } else {
        Ok(HttpResponse::Found()
            .append_header((LOCATION, "/login"))
            .finish())
    }
}

#[allow(dead_code)]
pub async fn handler_chemistry_activities(
    req: HttpRequest,
) -> Result<HttpResponse, UserFacingError> {
    if let Some(_) = req.get_session().get::<usize>("user_id").unwrap_or(None) {
        let file = actix_files::NamedFile::open("static/html/chemistry-activities.html").map_err(
            |_| {
                log::error!("Failed to open chemistry-activities.html file");
                UserFacingError::InternalError
            },
        )?;
        Ok(file.into_response(&req))
    } else {
        Ok(HttpResponse::Found()
            .append_header((LOCATION, "/login"))
            .finish())
    }
}

#[allow(dead_code)]
pub async fn handler_contact_us() -> Result<actix_files::NamedFile, UserFacingError> {
    actix_files::NamedFile::open("static/html/contact-us.html").map_err(|_| {
        log::error!("Failed to open contact-us.html file");
        UserFacingError::InternalError
    })
}

#[allow(dead_code)]
pub async fn handler_elements_lesson(req: HttpRequest, session: Session) -> Result<HttpResponse> {
    if let Some(_user_id) = req.get_session().get::<usize>("user_id").unwrap_or(None) {
        session.remove("activity_name");

        session.insert("activity_name", "elements_lesson")?;

        Ok(HttpResponse::SeeOther()
            .append_header((LOCATION, "/"))
            .finish())
    } else {
        Ok(HttpResponse::Found()
            .append_header((LOCATION, "/login"))
            .finish())
    }
}

#[allow(dead_code)]
pub async fn handler_game_algebra(session: Session) -> Result<HttpResponse> {
    if let Some(username) = session.get::<String>("username").unwrap_or(None) {
        if let Some(user_id) = session.get::<i32>("user_id").unwrap_or(None) {
            let external_base_link = "https://shawley-codes.github.io/AlgebraBlitz/";
            let external_link = format!(
                "{}?username={}?user_id={}",
                external_base_link, username, user_id
            );

            Ok(HttpResponse::Found()
                .append_header((LOCATION, external_link))
                .finish())
        } else {
            Ok(HttpResponse::Found()
                .append_header((LOCATION, "/login"))
                .finish())
        }
    } else {
        Ok(HttpResponse::Found()
            .append_header((LOCATION, "/login"))
            .finish())
    }
}

#[allow(dead_code)]
pub async fn handler_game_multiplication(session: Session) -> Result<HttpResponse> {
    if let Some(username) = session.get::<String>("username").unwrap_or(None) {
        if let Some(user_id) = session.get::<i32>("user_id").unwrap_or(None) {
            let external_base_link = "https://leemelzer.github.io/Game2-Host";
            let external_link = format!(
                "{}?username={}?user_id={}",
                external_base_link, username, user_id
            );

            Ok(HttpResponse::Found()
                .append_header((LOCATION, external_link))
                .finish())
        } else {
            Ok(HttpResponse::Found()
                .append_header((LOCATION, "/login"))
                .finish())
        }
    } else {
        Ok(HttpResponse::Found()
            .append_header((LOCATION, "/login"))
            .finish())
    }
}

#[allow(dead_code)]
pub async fn handler_forgot_password() -> Result<actix_files::NamedFile, UserFacingError> {
    actix_files::NamedFile::open("static/html/forgot-password.html").map_err(|_| {
        log::error!("Failed to open forgot-password.html file");
        UserFacingError::InternalError
    })
}

#[allow(dead_code)]
pub async fn handler_leaderboard(req: HttpRequest) -> Result<HttpResponse, UserFacingError> {
    if let Some(_) = req.get_session().get::<usize>("user_id").unwrap_or(None) {
        let file = actix_files::NamedFile::open("static/html/leaderboard.html").map_err(|_| {
            log::error!("Failed to open leaderboard.html file");
            UserFacingError::InternalError
        })?;
        Ok(file.into_response(&req))
    } else {
        Ok(HttpResponse::Found()
            .append_header((LOCATION, "/login"))
            .finish())
    }
}

#[allow(dead_code)]
pub async fn handler_login(req: HttpRequest) -> Result<actix_files::NamedFile, UserFacingError> {
    if let Some(_) = req.get_session().get::<usize>("user_id").unwrap_or(None) {
        actix_files::NamedFile::open("static/html/activities.html").map_err(|_| {
            log::error!("Failed to open activities.html file");
            UserFacingError::InternalError
        })
    } else {
        actix_files::NamedFile::open("static/html/login.html").map_err(|_| {
            log::error!("Failed to open login.html file");
            UserFacingError::InternalError
        })
    }
}

#[allow(dead_code)]
pub async fn handler_news() -> Result<actix_files::NamedFile, UserFacingError> {
    actix_files::NamedFile::open("static/html/news.html").map_err(|_| {
        log::error!("Failed to open news.html file");
        UserFacingError::InternalError
    })
}

#[allow(dead_code)]
pub async fn handler_not_found() -> Result<actix_files::NamedFile, UserFacingError> {
    actix_files::NamedFile::open("static/html/not-found.html").map_err(|_| {
        log::error!("Failed to open not-found.html file");
        UserFacingError::InternalError
    })
}

#[allow(dead_code)]
pub async fn handler_privacy_policy() -> Result<actix_files::NamedFile, UserFacingError> {
    actix_files::NamedFile::open("static/html/privacy-policy.html").map_err(|_| {
        log::error!("Failed to open privacy-policy.html file");
        UserFacingError::InternalError
    })
}

#[allow(dead_code)]
pub async fn handler_register(req: HttpRequest) -> Result<actix_files::NamedFile, UserFacingError> {
    if let Some(_) = req.get_session().get::<usize>("user_id").unwrap_or(None) {
        actix_files::NamedFile::open("static/html/account.html").map_err(|_| {
            log::error!("Failed to open activities.html file");
            UserFacingError::InternalError
        })
    } else {
        actix_files::NamedFile::open("static/html/register.html").map_err(|_| {
            log::error!("Failed to open login.html file");
            UserFacingError::InternalError
        })
    }
}

#[allow(dead_code)]
pub async fn handler_reset_password() -> Result<actix_files::NamedFile, UserFacingError> {
    actix_files::NamedFile::open("static/html/reset-password.html").map_err(|_| {
        log::error!("Failed to open reset-password.html file");
        UserFacingError::InternalError
    })
}

#[allow(dead_code)]
pub async fn handler_terms_conditions() -> Result<actix_files::NamedFile, UserFacingError> {
    actix_files::NamedFile::open("static/html/terms-conditions.html").map_err(|_| {
        log::error!("Failed to open terms-conditions.html file");
        UserFacingError::InternalError
    })
}

#[allow(dead_code)]
pub async fn serve_static_files(
    req: HttpRequest,
) -> Result<actix_files::NamedFile, UserFacingError> {
    let path: std::path::PathBuf = req.match_info().query("tail").parse().unwrap();
    let file = format!("static/{}", path.to_str().unwrap());

    actix_files::NamedFile::open(&file).map_err(|_| {
        log::error!("Failed to serve static file: {}", file);
        UserFacingError::InternalError
    })
}

#[allow(dead_code)]
pub async fn register(form: web::Form<RegisterFormData>, pool: web::Data<DbPool>) -> HttpResponse {
    match registration::register_user(form.into_inner(), &pool).await {
        Ok(user) => {
            log::info!("User {} registered successfully", user.username);
            HttpResponse::SeeOther()
                .append_header((LOCATION, "/login"))
                .finish()
        }
        Err(e) => {
            log::error!("Registration failed: {:?}", e);
            HttpResponse::BadRequest().body(format!("Registration failed"))
        }
    }
}

#[allow(dead_code)]
pub async fn authenticate(
    form: web::Form<LoginFormData>,
    pool: web::Data<DbPool>,
    session: Session,
) -> Result<HttpResponse, Error> {
    let login_result = authenticate::login_user(form.into_inner(), &pool).await;

    match login_result {
        Ok(user) => {
            session.remove("username");
            session.remove("user_id");
            session.remove("activity_name");

            session.insert("username", user.username)?;
            session.insert("user_id", user.id)?;

            Ok(HttpResponse::Found()
                .append_header((LOCATION, "/activities"))
                .finish())
        }
        Err(err) => {
            log::error!("Login failed: {:?}", err);

            Ok(HttpResponse::Unauthorized().body(format!("Login error")))
        }
    }
}

#[allow(dead_code)]
pub async fn logout(session: Session) -> impl Responder {
    session.purge();

    HttpResponse::Found()
        .append_header((LOCATION, "/login"))
        .finish()
}

#[allow(dead_code)]
pub async fn select_activity(
    activity_name: web::Path<String>,
    session: Session,
) -> Result<HttpResponse, UserFacingError> {
    session.remove("activity_name");
    session
        .insert("activity_name", activity_name.into_inner())
        .expect("TODO: panic message");

    Ok(HttpResponse::Ok().finish())
}

#[allow(dead_code)]
pub async fn get_username(session: Session) -> HttpResponse {
    let username = session.get::<String>("username").unwrap_or(None).unwrap();

    let response = UsernameResponse { username };

    HttpResponse::Ok().json(response)
}

#[allow(dead_code)]
pub async fn process_points_science(
    form: web::Form<PointsFormData>,
    pool: web::Data<DbPool>,
    session: Session,
) -> Result<HttpResponse, UserFacingError> {
    let _ = leaderboard_entry(form.into_inner(), &pool, &session).await;

    Ok(HttpResponse::Ok().finish())
}

#[allow(dead_code)]
pub async fn process_points(
    form: web::Form<MathFormData>,
    pool: web::Data<DbPool>,
    session: Session,
) -> Result<HttpResponse, UserFacingError> {
    let new_form = PointsFormData {
        game_name: form.game_name.to_string(),
        points: form.points,
    };

    session.remove("username");
    session.remove("user_id");
    let _ = session.insert("username", form.username.to_string());
    let _ = session.insert("user_id", form.user_id);

    let _ = leaderboard_entry(new_form, &pool, &session).await;

    let mut response = HttpResponse::Ok().finish();

    response.headers_mut().insert(
        header::ACCESS_CONTROL_ALLOW_ORIGIN,
        header::HeaderValue::from_static(
            "https://leemelzer.github.io/, https://shawley-codes.github.io/",
        ),
    );

    Ok(HttpResponse::Ok().finish())
}

#[allow(dead_code)]
pub async fn get_leaderboard_algebra(
    pool: web::Data<DbPool>,
) -> Result<HttpResponse, UserFacingError> {
    let subject = "game1";

    let players = retrieve_top_players(&subject, &pool).await?;

    Ok(HttpResponse::Ok().json(players))
}

#[allow(dead_code)]
pub async fn get_leaderboard_multiplication(
    req: HttpRequest,
    pool: web::Data<DbPool>,
) -> Result<HttpResponse, UserFacingError> {
    if let Some(_) = req.get_session().get::<usize>("user_id").unwrap_or(None) {
        let subject = "game2";

        let players = retrieve_top_players(&subject, &pool).await?;
        Ok(HttpResponse::Ok().json(players))
    } else {
        Ok(HttpResponse::Found()
            .append_header((LOCATION, "/login"))
            .finish())
    }
}

#[allow(dead_code)]
pub async fn get_leaderboard_science(
    req: HttpRequest,
    pool: web::Data<DbPool>,
) -> Result<HttpResponse, UserFacingError> {
    if let Some(_) = req.get_session().get::<usize>("user_id").unwrap_or(None) {
        let subject = "science";

        let players = retrieve_top_players(&subject, &pool).await?;
        Ok(HttpResponse::Ok().json(players))
    } else {
        Ok(HttpResponse::Found()
            .append_header((LOCATION, "/login"))
            .finish())
    }
}
