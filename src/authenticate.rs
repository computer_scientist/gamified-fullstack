use crate::database::forms::LoginFormData;
use crate::database::models::User;
// use crate::database::schema::users;
use crate::database::schema::users::dsl::*;
use crate::db::DbPool;
use crate::errors::LoginError;
use argon2::password_hash::{PasswordHash, PasswordVerifier};
use diesel::prelude::*;
use diesel::r2d2::{ConnectionManager, PooledConnection};
use diesel::PgConnection;

const MAX_FAILED_LOGINS: i32 = 7;

pub async fn login_user(user_data: LoginFormData, pool: &DbPool) -> Result<User, LoginError> {
    let mut connection = pool.get().map_err(|_| LoginError::DatabaseError)?;

    let user = match find_user(&user_data.username, &mut connection) {
        Ok(user) => user,
        Err(_) => {
            return Err(LoginError::InvalidCredentials);
        }
    };

    if user.failed_login_attempts >= MAX_FAILED_LOGINS {
        return Err(LoginError::TooManyFailedAttempts);
    }

    match verify_password(&user_data.password, &user.password) {
        Ok(_) => {
            reset_failed_logins(&user_data.username, &mut connection)?;
            Ok(user)
        }
        Err(_) => {
            increment_failed_logins(
                &user_data.username,
                user.failed_login_attempts,
                &mut connection,
            )?;
            Err(LoginError::InvalidCredentials)
        }
    }
}

fn find_user(
    other_username: &str,
    connection: &mut PooledConnection<ConnectionManager<PgConnection>>,
) -> Result<User, LoginError> {
    users
        .filter(username.eq(other_username))
        .first::<User>(connection)
        .map_err(|_| {
            log::error!("User not found when authenticating");
            LoginError::UserNotFound
        })
}

fn verify_password(other_password: &str, stored_password: &str) -> Result<(), LoginError> {
    let password_hash =
        PasswordHash::new(stored_password).map_err(|_| LoginError::InvalidCredentials)?;

    let verifier = argon2::Argon2::default();

    verifier
        .verify_password(other_password.as_bytes(), &password_hash)
        .map_err(|_| LoginError::InvalidCredentials)
}

fn increment_failed_logins(
    other_username: &str,
    failed_logins: i32,
    connection: &mut PooledConnection<ConnectionManager<PgConnection>>,
) -> Result<(), LoginError> {
    let target = users.filter(username.eq(other_username));

    diesel::update(target)
        .set(failed_login_attempts.eq(failed_logins + 1))
        .execute(connection)
        .map_err(|_| LoginError::DatabaseError)?;
    Ok(())
}

fn reset_failed_logins(
    other_username: &str,
    connection: &mut PooledConnection<ConnectionManager<PgConnection>>,
) -> Result<(), LoginError> {
    let target = users.filter(username.eq(other_username));
    diesel::update(target)
        .set(failed_login_attempts.eq(0))
        .execute(connection)
        .map_err(|_| LoginError::DatabaseError)?;
    Ok(())
}
