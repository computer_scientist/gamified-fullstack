use crate::handlers::{
    authenticate, get_leaderboard_algebra, get_leaderboard_multiplication, get_leaderboard_science,
    get_username, handler_about_us, handler_account, handler_activities,
    handler_biology_activities, handler_chemistry_activities, handler_contact_us,
    handler_elements_lesson, handler_forgot_password, handler_game_algebra,
    handler_game_multiplication, handler_index, handler_leaderboard, handler_login, handler_news,
    handler_privacy_policy, handler_register, handler_reset_password, handler_terms_conditions,
    logout, process_points, process_points_science, register,
};
use actix_web::web;

#[allow(dead_code)]
pub fn app_config(config: &mut web::ServiceConfig) {
    config
        .service(web::resource("/logout").route(web::get().to(logout)))
        // GET REQUESTS
        .service(web::resource("/").route(web::get().to(handler_index)))
        .service(web::resource("/about-us").route(web::get().to(handler_about_us)))
        .service(web::resource("/account").route(web::get().to(handler_account)))
        .service(web::resource("/activities").route(web::get().to(handler_activities)))
        .service(
            web::resource("/biology-activities").route(web::get().to(handler_biology_activities)),
        )
        .service(
            web::resource("/chemistry-activities")
                .route(web::get().to(handler_chemistry_activities)),
        )
        .service(web::resource("/contact-us").route(web::get().to(handler_contact_us)))
        .service(web::resource("/elements-lesson").route(web::get().to(handler_elements_lesson)))
        .service(web::resource("/forgot-password").route(web::get().to(handler_forgot_password)))
        .service(web::resource("/game-algebra").route(web::get().to(handler_game_algebra)))
        .service(
            web::resource("/game-multiplication").route(web::get().to(handler_game_multiplication)),
        )
        .service(web::resource("/leaderboard").route(web::get().to(handler_leaderboard)))
        .service(web::resource("/news").route(web::get().to(handler_news)))
        .service(web::resource("/privacy-policy").route(web::get().to(handler_privacy_policy)))
        .service(web::resource("/reset-password").route(web::get().to(handler_reset_password)))
        .service(web::resource("/terms-conditions").route(web::get().to(handler_terms_conditions)))
        .service(web::resource("/get-username").route(web::get().to(get_username)))
        .service(
            web::resource("/leaderboard-Algebra").route(web::get().to(get_leaderboard_algebra)),
        )
        .service(
            web::resource("/leaderboard-Multiplication")
                .route(web::get().to(get_leaderboard_multiplication)),
        )
        .service(
            web::resource("/leaderboard-Science").route(web::get().to(get_leaderboard_science)),
        )
        // Combos
        .service(
            web::resource("/register")
                .route(web::get().to(handler_register))
                .route(web::post().to(register)),
        )
        .service(
            web::resource("login")
                .route(web::get().to(handler_login))
                .route(web::post().to(authenticate)),
        )
        // POSTS
        .service(
            web::resource("/process_points_science").route(web::post().to(process_points_science)),
        )
        .service(web::resource("/process_points").route(web::post().to(process_points)))
        .service(
            web::resource("/{tail:.*}").route(web::get().to(crate::handlers::serve_static_files)),
        );
}
