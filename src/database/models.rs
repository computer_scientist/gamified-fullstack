use crate::database::schema::{leaderboard_points, users};
use diesel::{Insertable, Queryable};

#[derive(Debug, Queryable)]
pub struct User {
    pub id: i32,
    pub username: String,
    pub email: String,
    pub password: String,
    pub failed_login_attempts: i32,
}

#[derive(Insertable)]
#[diesel(table_name = users)]
pub struct NewUser {
    pub username: String,
    pub email: String,
    pub password: String,
    pub failed_login_attempts: i32,
}

#[derive(Queryable)]
pub struct LeaderboardPoints {
    pub user_id: i32,
    pub game1: i64,
    pub game2: i64,
    pub science: i64,
}

#[derive(Insertable)]
#[diesel(table_name = leaderboard_points)]
pub struct InsertLeaderboardPoints {
    pub game1: i64,
    pub game2: i64,
    pub science: i64,
}

#[derive(Queryable)]
pub struct Lesson {
    pub id: i32,
    pub name: String,
}

#[derive(Queryable)]
pub struct Practice {
    pub id: i32,
    pub name: String,
}

#[derive(Queryable)]
pub struct UserLesson {
    pub user_id: i32,
    pub lesson_id: Option<i32>,
    pub completion_date: Option<chrono::DateTime<chrono::Utc>>,
}

#[derive(Queryable)]
pub struct UserPractice {
    pub user_id: i32,
    pub practice_id: Option<i32>,
    pub completion_date: Option<chrono::DateTime<chrono::Utc>>,
}

#[derive(Queryable)]
pub struct Point {
    pub user_id: i32,
    pub points_earned: i64,
}
