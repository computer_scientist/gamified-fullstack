// @generated automatically by Diesel CLI.

diesel::table! {
    leaderboard_points (user_id) {
        user_id -> Int4,
        game1 -> Int8,
        game2 -> Int8,
        science -> Int8,
    }
}

diesel::table! {
    lessons (id) {
        id -> Int4,
        name -> Varchar,
    }
}

diesel::table! {
    points (user_id) {
        user_id -> Int4,
        points_earned -> Nullable<Int8>,
    }
}

diesel::table! {
    practices (id) {
        id -> Int4,
        name -> Varchar,
    }
}

diesel::table! {
    user_lessons (user_id) {
        user_id -> Int4,
        lesson_id -> Nullable<Int4>,
        completion_date -> Nullable<Timestamptz>,
    }
}

diesel::table! {
    user_practices (user_id) {
        user_id -> Int4,
        practice_id -> Nullable<Int4>,
        completion_date -> Nullable<Timestamptz>,
    }
}

diesel::table! {
    users (id) {
        id -> Int4,
        username -> Varchar,
        email -> Varchar,
        password -> Varchar,
        failed_login_attempts -> Int4,
    }
}

diesel::joinable!(leaderboard_points -> users (user_id));
diesel::joinable!(points -> users (user_id));
diesel::joinable!(user_lessons -> lessons (lesson_id));
diesel::joinable!(user_lessons -> users (user_id));
diesel::joinable!(user_practices -> practices (practice_id));
diesel::joinable!(user_practices -> users (user_id));

diesel::allow_tables_to_appear_in_same_query!(
    leaderboard_points,
    lessons,
    points,
    practices,
    user_lessons,
    user_practices,
    users,
);
