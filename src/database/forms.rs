use diesel::Queryable;
use serde::{Deserialize, Serialize};

#[derive(Debug, Deserialize)]
pub struct RegisterFormData {
    pub(crate) email: String,
    pub(crate) confirm_email: String,
    pub(crate) username: String,
    pub(crate) password: String,
    pub(crate) confirm_password: String,
}

#[derive(Debug, Deserialize)]
pub struct LoginFormData {
    pub(crate) username: String,
    pub(crate) password: String,
}

#[derive(Debug, Deserialize)]
pub struct MathFormData {
    pub(crate) user_id: i32,
    pub(crate) username: String,
    pub(crate) game_name: String,
    pub(crate) points: i64,
}

#[derive(Serialize)]
pub struct UsernameResponse {
    pub(crate) username: String,
}

#[derive(Debug, Deserialize)]
pub struct PointsFormData {
    pub(crate) game_name: String,
    pub(crate) points: i64,
}

#[derive(Queryable, Serialize)]
pub struct PlayerPoints {
    pub(crate) username: String,
    pub(crate) points: i64,
}
