use diesel::r2d2::{ConnectionManager, Pool};
use diesel::PgConnection;
use dotenvy::dotenv;
use std::env;

pub type DbPool = Pool<ConnectionManager<PgConnection>>;

#[allow(dead_code)]
pub fn create_pool() -> DbPool {
    dotenv().ok();

    let manager = ConnectionManager::<PgConnection>::new(
        env::var("DATABASE_URL").expect("DATABASE_URL must be set"),
    );
    Pool::builder()
        .build(manager)
        .expect("Failed to create pool.")
}
