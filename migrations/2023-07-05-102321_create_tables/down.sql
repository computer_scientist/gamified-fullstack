--
-- My Queries
--
DROP TABLE points;
DROP TABLE user_practices;
DROP TABLE user_lessons;
DROP TABLE practices;
DROP TABLE lessons;
DROP TABLE users;
DROP TABLE leaderboard_points;