--
-- My Queries
--
CREATE TABLE users
(
    id       SERIAL PRIMARY KEY,
    username VARCHAR NOT NULL,
    email    VARCHAR NOT NULL,
    password VARCHAR NOT NULL,
    failed_login_attempts INTEGER NOT NULL DEFAULT 0
);

CREATE TABLE lessons
(
    id   SERIAL PRIMARY KEY,
    name VARCHAR NOT NULL
);

CREATE TABLE practices
(
    id   SERIAL PRIMARY KEY,
    name VARCHAR NOT NULL
);

CREATE TABLE user_lessons
(
    user_id         INTEGER PRIMARY KEY REFERENCES users (id),
    lesson_id       INTEGER REFERENCES lessons (id),
    completion_date TIMESTAMP WITH TIME ZONE
);

CREATE TABLE user_practices
(
    user_id         INTEGER PRIMARY KEY REFERENCES users (id),
    practice_id     INTEGER REFERENCES practices (id),
    completion_date TIMESTAMP WITH TIME ZONE
);

CREATE TABLE points
(
    user_id       INTEGER PRIMARY KEY REFERENCES users (id),
    points_earned BIGINT
);

CREATE TABLE leaderboard_points
(
    user_id INTEGER PRIMARY KEY REFERENCES users (id),
    game1   BIGINT NOT NULL,
    game2   BIGINT NOT NULL,
    science BIGINT NOT NULL
);