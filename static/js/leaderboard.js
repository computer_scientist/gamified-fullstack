document.addEventListener("DOMContentLoaded", () => {
    const leaderboardTable = document.getElementById("leaderboard-table");
    const subjectSelector = document.getElementById("subject-selector");

    subjectSelector.addEventListener("change", (e) => {
        const selectedSubject = e.target.value;

        fetch(`/leaderboard-${selectedSubject}`)
            .then((response) => response.json())
            .then((data) => populateLeaderboard(data))
            .catch((err) => console.error(err));
    });

    const populateLeaderboard = (data) => {
        // Clear existing rows
        while (leaderboardTable.rows.length > 1) {
            leaderboardTable.deleteRow(1);
        }
        // Add new rows
        data.forEach((player) => {
            const row = leaderboardTable.insertRow(-1);
            const cell1 = row.insertCell(0);
            const cell2 = row.insertCell(1);
            cell1.textContent = player.username;
            cell2.textContent = player.points;
        });
    };
});
