function displayUsername() {
    fetch("/get-username")
        .then((response) => response.json())
        .then((data) => {
            const usernameElement = document.getElementById(
                "username-placeholder"
            );
            if (data && data.username) {
                usernameElement.textContent = `User: ${data.username}`;
            } else {
                usernameElement.textContent = "Failed to Display Username";
            }
        })
        .catch((error) => {
            console.error("Error fetching username", error);
        });
}

displayUsername();
